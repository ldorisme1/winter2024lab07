public class Card{
	
	// the one with the highest value = win first if same suits
    private String suits;
    private int value;

    public Card(String theSuit, int theValue){
		this.suits = theSuit;
		this.value = theValue;
	}

    // getter method to return our field
    public String getSuit() {
        return this.suits;

    }

    public int getValue() {
        return this.value;
    }

    public String toString() {
        return this.suits + " of  " + this.value;
    }

    public double calculateScore(){
        double rank = this.value;
        double scoreAdded = 0;
        if (this.suits.equals("Hearts")) {
            scoreAdded = 0.4;
        } else if (this.suits.equals("Diamonds")) {
            scoreAdded = 0.3;
        } else if (this.suits.equals("Spades")) {
            scoreAdded = 0.2;
        } else if (this.suits.equals("Clubs")) {
            scoreAdded = 0.1;
        }
        return rank + scoreAdded;
    }
}